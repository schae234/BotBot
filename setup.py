#!/usr/bin/env python

from distutils.core import setup

setup(name='BotBot',
      version='0.0.1',
      description='Laboratory computational resource management',
      author='Rob Schaefer',
      author_email='schae234@umn.edu',
      url='http://csbio.cs.umn.edu/schae234',
      packages=[],
     )
